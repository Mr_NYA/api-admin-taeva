
const mysqldump = require('mysqldump');
const mysqlBackup = require('mysql-backup');
const multer = require('multer');
const upload = multer({ dest: '/home/files'});
const fs = require('fs');
const express = require('express');
const { spawn, execSync } = require('child_process');

const chatbotName =  'chatbotsenior_chatbot_1';

// Set up the express app
const app = express();

const PORT = 55000;


//CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}


app.use(allowCrossDomain)
app.use(express.static('public'))


app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
});


app.get('/dev', (req, res) => {
    var promise = dumpDevB(res);
    console.log(promise);
})


app.get('/api', (req, res) => {
  	const child = spawn('docker', ['restart', chatbotName]);
	const { exec } = require('child_process');
//exec('docker restart '+chatbotName);
	//exec('docker',['docker', 'exec', '-it', chatbotName,'//bin//bash']);
       	//exec('./mvnw','-Ptest');
  // use child.stdout.setEncoding('utf8'); if you want text chunks
  child.stdout.setEncoding('utf8');
  res.set('Content-Type', 'text/html');
  child.stdout.on('data', (chunk) => {
      	console.log(`stdout: ${chunk}`);
//	exec('docker',['docker', 'exec', '-it', chatbotName,'//bin//bash']);
//	exec('./mvnw','-Ptest');

      res.write(chunk);
  });

  child.on('close', (code) => {
  
   exec('docker',['docker', 'exec', '-it', chatbotName,'//bin//bash']);
        exec('./mvnw','-Ptest');

    //console.log(`child process exited with code ${code}`);
        if (code === 0) {
            res.end();
        } else {
            console.log("Error Command returned code: " + code);
        }
  });

});

app.post('/graph', upload.any(), function (req, res, next) {
    console.log("REQUEST: /graph");
	try{
	let Error=null;
	for(let i=0;i<req.files.length;i++){
	const file = req.files[i];
        fs.rename('/home/files/' + file.filename, '/home/files/'+file.originalname, function (err) {
            try{
		if (err) {
                //res.status(500).send("An error happened.");
                res.write("An error happended while renaming the file.");
                console.log("An error happended while renaming the file.");
                console.log(err);
		Error=err;
                return;
            }{
            console.log('renamed '+ file.filename +' into ' + file.originalname);
            var command = 'docker cp /home/files/'+file.originalname+' '+chatbotName+':/root/workspace/Unitex-GramLab/Unitex/French/Graphs/'+file.originalname;
            var stdout = execSync(command);
            //console.log(`stdout: ${stdout}`);
            var stdout2 = execSync('rm /home/files/'+file.originalname);
            console.log(`stdout: ${stdout2}`);
		}
		}catch(err2){
		Error = err2;
		}
        });
    }

	if(Error!=null){
	   res.status(200).send(req.files);
	}else{
	   res.status(201).send(Error);
	}
	
	

	}catch(err){
	res.status(401).send(err);
	}

});

app.post('/dico', upload.any(), function (req, res, next) {
    console.log("REQUEST: /dico");
    req.files.forEach(function (file) {
        fs.rename('/home/files/' + file.filename, '/home/files/'+file.originalname, function (err) {
            if (err) {
                //res.status(500).send("An error happened.");
                res.write("An error happended while renaming the file.");
                console.log("An error happended while renaming the file.");
                console.log(err);
                return;
            };
            console.log('renamed '+ file.filename +' into ' + file.originalname);
            var command = 'docker cp /home/files/'+file.originalname+' '+chatbotName+':/root/workspace/Unitex-GramLab/Unitex/French/Dela/';
            var stdout = execSync(command);
            var stdout2 = execSync('rm /home/files/'+file.originalname);

            console.log(`stdout: ${stdout}`);
            console.log(`stdout2: ${stdout2}`);
        });
    });

    res.status(200).send(req.files);
});


app.get('/state', (req, res) => {

try{
 
 const child = spawn('docker', [ 'inspect', "--format='{{.State.Status}} {{.State.Health.Status}}'", chatbotName]);

  // use child.stdout.setEncoding('utf8'); if you want text chunks
  child.stdout.setEncoding('utf8');
  res.set('Content-Type', 'text/html');
  child.stdout.on('data', (chunk) => {
      console.log(`stdout: ${chunk}`);
      res.write(chunk);
  });

  child.on('close', (code) => {
      console.log(`child process exited with code ${code}`);
      if (code === 0) {
        res.end();
      } else {
          console.log("Error Command returned code: " + code);     
}
  });
}catch(err){
	res.status(401).send(err);
}

});


function dumpDev(){
    var path = './dumpDev' + getDateTime() + '.sql';
    try {
        mysqldump({
            connection: {
                host: '35.197.203.96',
                user: 'taeva',
                password: 'PovSkT9uk6WBhI0kOxwj',
                database: 'taeva_dev',
            },
            dumpToFile: path,
        });
        return path;
    } catch (error) {
        console.log(error);
        res.send(error);
    }
}

function dumpDevB(res){
    var path = './dumpDev' + getDateTime() + '.sql';
    try {
        console.log('database dumping');
        mysqldump({
          connection: {
            host: '35.197.203.96',
            user: 'taeva',
            password: 'PovSkT9uk6WBhI0kOxwj',
            database: 'taeva_dev',
          }
        }).then(dump => {
            try {
                console.log(dump);
                fs.writeFile(path, dump, function (err) {
                    if(err)throw err;
                    res.download(path);
                });
            } catch (error) {
                console.log('There is an error in then(): ' +error);
                res.send(error);                
            }
        });
    } catch (error) {
        console.log(error);
        res.send(error);
    }
}

function getDateTime() {
    var date = new Date();
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    return year + "_" + month + "_" + day + "_" + hour + "_" + min + "_" + sec;
}

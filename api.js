
const mysqldump = require('mysqldump');
const mysqlBackup = require('mysql-backup');
const multer = require('multer');
const upload = multer({ dest: '/home/files'});
const fs = require('fs');
const express = require('express');
const { spawn, execSync, exec } = require('child_process');
var http = require('http');
var request = require('request');
const shell = require('shelljs')

const chatbotNameDev = 'chatbot-senior-chatbot-1'
const chatbotNameTest =  'chatbottaeva_chatbot-test_1';
const chatbotNameProd = 'chatbottaeva_chatbot_prod_1'

// Set up the express app
const app = express();

const PORT = 55000;
 

//CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}


app.use(allowCrossDomain)
app.use(express.static('public'))


app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
});


app.get('/dev', (req, res) => {
    var promise = dumpDevB(res);
    console.log(promise);
})

app.post('/fleet-logs/setListeToken', upload.any(), function(req, res) {
    console.log("In fleet-logs setListeToken");
    try{
     
        let data = '';
        req.on('data', chunk => {
            data += chunk;
          })
          req.on('end', () => {
            if(data !=  null && data != '')
            {
                console.log(data)
                request.post({
                    url:     'http://localhost:8060/setListeToken',
                    body: JSON.parse(data) ,
                    json: true
                }
                , (error, response, body)=>{
                    res.json(body).send();
                  }
                )
            }
            else{
                res.send("Body is empty")
            }
        })
    }
    catch{

    }
})

app.post('/fleet-logs/setListeTokenWithDate', upload.any(), function(req, res) {
    console.log("In fleet-logs setListeToken");
    try{
     
        let data = '';
        req.on('data', chunk => {
            data += chunk;
          })
          req.on('end', () => {
            if(data !=  null && data != '')
            {
                console.log(data)
                request.post({
                    url:     'http://localhost:8060/setListeTokenWitheDate',
                    body: JSON.parse(data) ,
                    json: true
                }
                , (error, response, body)=>{
                    res.json(body).send();
                  }
                )
            }
            else{
                res.send("Body is empty")
            }
        })
    }
    catch{

    }
})


app.post('/sendMessageProd', upload.any(), function(req, res, next){
    console.log('In sendMessageProd');

    try{
     
        let data = '';
        req.on('data', chunk => {
            data += chunk;
          })
          req.on('end', () => {
            if(data !=  null && data != '')
            {
                console.log(data)
                request.post({
                    url:     'http://localhost:8085/api/ChatbotAPI',
                    body: JSON.parse(data) ,
                    json: true
                }
                , (error, response, body)=>{
                    res.json(body).send();
                  }
                )
            }
        })
    }
    catch{

    }
})


app.post('/sendMessageTest', upload.any(), function(req, res, next){
    console.log('In sendMessageTest');

    try{
     
        let data = '';
        req.on('data', chunk => {
            data += chunk;
          })
          req.on('end', () => {
            if(data !=  null && data != '')
            {
                console.log(data)
                request.post({
                    url:     'http://localhost:8086/api/ChatbotAPI',
                    body: JSON.parse(data) ,
                    json: true
                }
                , (error, response, body)=>{
                    res.json(body).send();
                  }
                )
            }
        })
    }
    catch{

    }
})

app.post('/lemmatisationTest', upload.any(), function(req, res, next){
    console.log('In lemmatisationTest');

    try{
     
        const start = req.query.start;
        const end = req.query.end;

        console.log("start ==> " , start)
        console.log("end ==> " , end)
                
                request.post({
                    headers: {'content-type' : 'application/x-www-form-urlencoded'},
                    url:     'http://localhost:8086/api/Lemmatisation?start='+start+'&end='+ end,
                }
                , function(error, response, body){
                    console.log(body);
                    res.json(body).send();
                  }
                )
    }
    catch{

        console.log('error')

    }
})


app.post('/lemmatisationVerbeTest', upload.any(), function(req, res, next){
    console.log('In lemmatisationVerbeTest');

    try{
     
        const start = req.query.start;
        const end = req.query.end;

        console.log("start ==> " , start)
        console.log("end ==> " , end)
                
                request.post({
                    headers: {'content-type' : 'application/x-www-form-urlencoded'},
                    url:     'http://localhost:8086/api/LemmatisationVerbe?start='+start+'&end='+ end,
                }
                , function(error, response, body){
                    console.log(body);
                    res.json(body).send();
                  }
                )
    }
    catch{

        console.log('error')

    }
})


app.post('/clone_DB', upload.any(), function(req, res, next){
    console.log('In clone_DB');
    try{
        shell.exec('/home/teamnet_production/Clone_DB/clone_db.sh');
        res.write("lancement du script du clonage de BD avec SUCCES");
        res.end();
    }
    catch(err){
        console.log(err);
        res.write(err);
        res.end();
    }

});

app.post('/sendConfig', upload.any(), function(req, res, next){
    console.log('In sendConfig');

    try{
        let usedChatBot = selectedENV(req);
        console.log('usedChatBot ==> ' + usedChatBot)
        
        console.log(req.files.length)
        if(req.files.length > 0)
        {
            console.log('file founded')
            res.write('file founded : ' + req.files.length);
            res.end();

            const file = req.files[0];
            console.log(file.originalname)
            fs.rename('/home/files/' + file.filename, '/home/files/'+file.originalname, function (err) {
                console.log('renamed '+ file.filename +' into ' + file.originalname);
                var command = 'mv /home/files/'+file.originalname + ' /home/teamnet_production/Config_TAEVA/';
                execSync(command)
                var commandMoveConfig = 'docker cp /home/teamnet_production/Config_TAEVA/Config ' + usedChatBot + ':/root/workspace/Unitex-GramLab/Unitex/French/Config'
                execSync(commandMoveConfig)
            });
        }

    }
    catch(err){
        console.log(err);
        res.write(err);
        res.end();
    }

})

app.get('/listGraphRoot', upload.any(), function (req, res, next){
    let usedChatBot = selectedENV(req);
    console.log('usedChatBot ==> ' + usedChatBot)
    exec("docker exec  "+ usedChatBot +" ls /root/workspace/Unitex-GramLab/Unitex/French/Graphs -la",(error, stdout, stderr) =>{
        if (error) {
            console.log(`error: ${error.message}`);
            res.write("\n \n ERROR ERROR ERROR \n \n ERROR ERROR ERROR");
            res.end();
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log("List graph from /root");
        console.log(`stdout: ${stdout}`);
        res.write(stdout);
        res.end();
      } );
})

app.get('/listGraphCode', upload.any(), function (req, res, next){
    let usedChatBot = selectedENV(req);
    console.log('usedChatBot ==> ' + usedChatBot)
    exec("docker exec  "+ usedChatBot +" ls /code/workspace/Unitex-GramLab/Unitex/French/Graphs -la",(error, stdout, stderr) =>{
        if (error) {
            console.log(`error: ${error.message}`);
            res.write("\n \n ERROR ERROR ERROR \n \n ERROR ERROR ERROR");
            res.end();
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log("List graph from /code");
        console.log(`stdout: ${stdout}`);
        res.write(stdout);
        res.end();
      } );
})


app.get('/listDictionnary', upload.any(), function (req, res, next){
    let usedChatBot = selectedENV(req);
    console.log('usedChatBot ==> ' + usedChatBot)
    exec("docker exec  "+ usedChatBot +" ls /root/workspace/Unitex-GramLab/Unitex/French/Dela -la",(error, stdout, stderr) =>{
        if (error) {
            console.log(`error: ${error.message}`);
            res.write("\n \n ERROR ERROR ERROR \n \n ERROR ERROR ERROR");
            res.end();
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log("List dictionnary from /root");
        console.log(`stdout: ${stdout}`);
        res.write(stdout);
        res.end();
      } );
})

app.post('/unzipDictionnary', upload.any(), function (req, res, next) {
    
    console.log("In Unzip");

    try{
        let usedChatBot = selectedENV(req);
        console.log('usedChatBot ==> ' + usedChatBot)
        console.log(req.files.length)
        for(let i = 0; i < req.files.length; i++){
            const file = req.files[i];
            console.log("file.path "+file.path)
            console.log("file.filename ",file.filename)
            console.log("file.originalname ",file.originalname)
            res.write(file.originalname)
             fs.rename('/home/files/' + file.filename, '/home/files/'+file.originalname, function (err) {
                console.log('renamed '+ file.filename +' into ' + file.originalname);
                var command = 'mv /home/files/'+file.originalname + ' /home/teamnet_production/dictionnary_zipped/';
                execSync(command)
                var commandRemoveDictionnary = 'rm -rf /home/teamnet_production/dictionnary/*'
                execSync(commandRemoveDictionnary);
                console.log("remove done")
                var commandUnzip = 'unzip /home/teamnet_production/dictionnary_zipped/'+file.originalname + ' -d /home/teamnet_production/dictionnary';
                var stdout = execSync(commandUnzip)
                console.log(`stdout: ${stdout}`);
                var commandRemoveDela = 'docker exec ' + usedChatBot + ' rm -rf /root/workspace/Unitex-GramLab/Unitex/French/Dela/';
                execSync(commandRemoveDela);
                var commandMoveDela = 'docker cp /home/teamnet_production/dictionnary/Dela/ ' + usedChatBot + ':/root/workspace/Unitex-GramLab/Unitex/French/'
                execSync(commandMoveDela);
            })
            
            
            res.end();
        }
    }
    catch(err){
        console.log(err)
    }
})


app.get('/api', (req, res) => {
    let usedChatBot = selectedENV(req);
    console.log('usedChatBot ==> ' + usedChatBot)
  	const child = spawn('docker', ['restart', usedChatBot]);
	const { exec } = require('child_process');

  // use child.stdout.setEncoding('utf8'); if you want text chunks
  child.stdout.setEncoding('utf8');
  res.set('Content-Type', 'text/html');
  child.stdout.on('data', (chunk) => {
      	console.log(`stdout: ${chunk}`);
      res.write(chunk);

      exec("docker exec  "+ usedChatBot +" nohup ./run.sh &",(error, stdout, stderr) =>{
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log("start Taeva server");
        console.log(`stdout: ${stdout}`);
   
      } );
  });

  child.on('close', (code) => {
  

    console.log(`child process exited with code ${code}`);
        if (code === 0) {
            res.end();
        } else {
            console.log("Error Command returned code: " + code);
        }
  });

});

app.post('/graph', upload.any(), function (req, res, next) {
    console.log("REQUEST: /graph");
	try{
        let usedChatBot = selectedENV(req);
        console.log('usedChatBot ==> ' + usedChatBot)
	let Error=null;
	for(let i=0;i<req.files.length;i++){
	const file = req.files[i];
        fs.rename('/home/files/' + file.filename, '/home/files/'+file.originalname, function (err) {
            try{
		if (err) {
                //res.status(500).send("An error happened.");
                res.write("An error happended while renaming the file.");
                console.log("An error happended while renaming the file.");
                console.log(err);
		Error=err;
                return;
            }{
            console.log('renamed '+ file.filename +' into ' + file.originalname);
            var command = 'docker cp /home/files/'+file.originalname+' '+usedChatBot+':/root/workspace/Unitex-GramLab/Unitex/French/Graphs/'+file.originalname;
            console.log("Before modify graphe");
            var stdout = execSync(command);
            //console.log(`stdout: ${stdout}`);
            var stdout2 = execSync('rm /home/files/'+file.originalname);
            console.log(`stdout: ${stdout2}`);
		}
		}catch(err2){
		Error = err2;
		}
        });
    }

	if(Error!=null){
	   res.status(200).send(req.files);
	}else{
	   res.status(201).send(Error);
	}
	
	

	}catch(err){
	res.status(401).send(err);
    console.log(err)
	}

});

app.post('/dico', upload.any(), function (req, res, next) {
    console.log("REQUEST: /dico");
    let usedChatBot = selectedENV(req);
    console.log('usedChatBot ==> ' + usedChatBot)
    req.files.forEach(function (file) {
        fs.rename('/home/files/' + file.filename, '/home/files/'+file.originalname, function (err) {
            if (err) {
                //res.status(500).send("An error happened.");
                res.write("An error happended while renaming the file.");
                console.log("An error happended while renaming the file.");
                console.log(err);
                return;
            };
            console.log('renamed '+ file.filename +' into ' + file.originalname);
            var command = 'docker cp /home/files/'+file.originalname+' '+usedChatBot+':/root/workspace/Unitex-GramLab/Unitex/French/Dela/';
            var stdout = execSync(command);
            var stdout2 = execSync('rm /home/files/'+file.originalname);

            console.log(`stdout: ${stdout}`);
            console.log(`stdout2: ${stdout2}`);
        });
    });

    res.status(200).send(req.files);
});


app.get('/state', (req, res) => {

try{
    let usedChatBot = selectedENV(req);
    console.log('usedChatBot ==> ' + usedChatBot)
 
 const child = spawn('docker', [ 'inspect', "--format='{{json .State.Status}}'", usedChatBot]);

  // use child.stdout.setEncoding('utf8'); if you want text chunks
  child.stdout.setEncoding('utf8');
  res.set('Content-Type', 'text/html');
  child.stdout.on('data', (chunk) => {
      console.log(`stdout: ${chunk}`);
      res.write(chunk);
  });

  child.on('close', (code) => {
      console.log(`child process exited with code ${code}`);
      if (code === 0) {
        res.end();
      } else {
          console.log("Error Command returned code: " + code); 
          res.write('ERROR'); 
          res.end();   
}
  });
}catch(err){
	res.status(401).send(err);
}

});

app.get('/GetLog',  (req, res) => {
    let input = req.query.input;
    console.log("recevoir la demande sur les logs de " + input)
    http.get('http://localhost:8086/api/GetLog?input=' + input,  (resp)=>{
    let data = '';
    console.log('envoyer la demande au serveur de log')
     // Un morceau de réponse est reçu
    resp.on('data', (chunk) => {
    data += chunk;
    });
    resp.on('end', () => {
        if(data != null && data != ''){
            console.log('recevoir la demande du serveur de log')
            body = JSON.parse(data)
            res.json(body).send()
        }
        else{
            res.send('data not found')
        }
       
      });
    
    })
})

function selectedENV(req){
    let usedChatBot = chatbotNameTest
    switch(req.query.env)
    {
        case 'PROD' : 
        console.log('env ==> ' + req.query.env);
        usedChatBot = chatbotNameProd;
        break;
        case 'TEST' : 
        console.log('env ==> ' + req.query.env);
        usedChatBot = chatbotNameTest;
        break;
        case 'DEV' : 
        console.log('env ==> ' + req.query.env);
        usedChatBot = chatbotNameDev;
        break;
        default : 
        usedChatBot = chatbotNameTest;
        console.log('default chatbot ' + usedChatBot)
    }
    return usedChatBot
}

function dumpDev(){
    var path = './dumpDev' + getDateTime() + '.sql';
    try {
        mysqldump({
            connection: {
                host: '35.197.203.96',
                user: 'taeva',
                password: 'PovSkT9uk6WBhI0kOxwj',
                database: 'taeva_dev',
            },
            dumpToFile: path,
        });
        return path;
    } catch (error) {
        console.log(error);
        res.send(error);
    }
}

function dumpDevB(res){
    var path = './dumpDev' + getDateTime() + '.sql';
    try {
        console.log('database dumping');
        mysqldump({
          connection: {
            host: '35.197.203.96',
            user: 'taeva',
            password: 'PovSkT9uk6WBhI0kOxwj',
            database: 'taeva_dev',
          }
        }).then(dump => {
            try {
                console.log(dump);
                fs.writeFile(path, dump, function (err) {
                    if(err)throw err;
                    res.download(path);
                });
            } catch (error) {
                console.log('There is an error in then(): ' +error);
                res.send(error);                
            }
        });
    } catch (error) {
        console.log(error);
        res.send(error);
    }
}

function getDateTime() {
    var date = new Date();
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    return year + "_" + month + "_" + day + "_" + hour + "_" + min + "_" + sec;
}
